import os
import sys

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "secure_messages.settings_prod")
sys.path.append("/var/www/secure_messages/cs-group1/")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
