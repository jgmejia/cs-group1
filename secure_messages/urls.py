"""secure_messages URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from inbox_messages import views as messages_views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    #    url(r'^login/$', auth_views.login, name='login'),
    #    url(r'^logout/$', auth_views.logout, name='logout'),
    url(r'^accounts/', include('registration.backends.hmac.urls')),
    url(r'^inbox/$', messages_views.inbox, name='inbox'),
    url(r'^dumpdb/$', messages_views.dump_db, name='dumpdb'),
    url(r'^dbdump/$', messages_views.dump_db, name='dumpdb'),
    url(r'^send_messages/$', messages_views.send_messages, name='send_messages'),
    url(r'^index/$', auth_views.login, name='default'),
    url(r'^index\.html/$', auth_views.login, name='default'),
    url(r'^$', auth_views.login, name='default'),
]
