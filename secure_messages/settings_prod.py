from .settings import *

EMAIL_HOST = 'smtp.zoho.com'
EMAIL_PORT = 465
EMAIL_HOST_USER = 'csgroup1@zoho.com'
EMAIL_HOST_PASSWORD = 'p0o9i8u7y6'
EMAIL_USE_SSL = True

DEFAULT_FROM_EMAIL = 'csgroup1@zoho.com'

ALLOWED_HOSTS.append('146.196.54.128')
ALLOWED_HOSTS.append('server918250.unmui.com')

DEBUG = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'secure_messages_db',
        'USER': 'secure_messages_user',
        'PASSWORD': 'secure_messages_pw',
        'HOST': 'localhost',
        'PORT': '3306',
    }
}

