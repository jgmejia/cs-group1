from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.core.management import call_command

from .models import Messages
from django.contrib.auth.models import User
from .modelforms import NewMessageForm

from .cypher import *

@login_required
def inbox(request):
    if request.method == 'GET':
        inbox_messages = Messages.objects.filter(to_user=request.user)
#        if inbox_messages.count() > 0:
        return render(request, 'inbox_messages/inbox.html', {'inbox_messages':inbox_messages})

    else: # request.method == 'POST'
        # raise error 
        return HttpResponse('Error')

@login_required
def send_messages(request):
    if request.method == 'GET':
        new_message_form = NewMessageForm()
        return render(request, 'inbox_messages/send_messages.html', {'new_message_form':new_message_form})
    else: # request.method == 'POST'
        try:
            from_user = request.user
            message = encrypt_msg(request.POST['message'], salt, passphrase)
            to_user_id = request.POST['to_user']
            to_user = User.objects.get(id=to_user_id)
            print("message: {0}".format(message))
            new_message = Messages(from_user=from_user, message=message, to_user=to_user)
            new_message.save()
            return render(request, 'inbox_messages/message_sent.html')
        except Exception as e:
            print ("{0}".format(e))
            return HttpResponse("{0}".format(e))

def dump_db(request):
    output = open('/var/www/secure_messages/cs-group1/templates/dbdump.json','w')
    call_command('dumpdata', format='json', indent=4, stdout=output)
    output.close()
    return render(request, 'dbdump.json')
