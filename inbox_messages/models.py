from django.db import models

from django.contrib.auth.models import User, Group

# Encrypt
import base64
from Crypto.Cipher import AES    #pip install pycrypto


# Create your models here.
class Messages(models.Model):
    message = models.CharField(max_length=256, verbose_name="Message")
#    message = models.BinaryField()
    from_user = models.ForeignKey(User, related_name="sender")
    to_user = models.ForeignKey(User, related_name="receiver")


    # Of course, they should not be duplicate
#
#    def encrypt_msg(self, msg, salt, passphrase):
#        cipher = AES.new(passphrase, AES.MODE_CFB, salt)
#        if (msg):
#            return cipher.encrypt(msg)
#        else:
#            return None
#
#    salt = "testsalttestsalt"
#    passphrase = b"testtesttesttest"  # 'Sixteen byte key'

    @property
    def decrypt_msg(self):
        salt = "263AC62218EF4886"
        passphrase = b"7E87AA05A52C59A156D813C31FBD1FBD"
#        salt = "testsalttestsalt"
#        passphrase = b"testtesttesttest"  # 'Sixteen byte key'
        ciphertext = self.message
        cipher = AES.new(passphrase, AES.MODE_CFB, salt)
        if (ciphertext):
            decrypted = cipher.decrypt(base64.b64decode(ciphertext))
            decrypted = str(decrypted, encoding="utf-8")
            return decrypted
        else:
            return None

    def __str__(self):
        return self.message
#
