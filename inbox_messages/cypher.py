# Encrypt
import base64
from Crypto.Cipher import AES    #pip install pycrypto
   
salt = "263AC62218EF4886"
passphrase = b"7E87AA05A52C59A156D813C31FBD1FBD"    #'Sixteen byte key'

def encrypt_msg(msg, salt, passphrase):
    cipher = AES.new(passphrase, AES.MODE_CFB, salt)
    if (msg):
        return base64.b64encode(cipher.encrypt(msg))
    else:
        return None

def decrypt_msg(ciphertext, salt, passphrase):
    cipher = AES.new(passphrase, AES.MODE_CFB, salt)
    if (ciphertext):
        decrypted = cipher.decrypt(base64.b64decode(ciphertext))
        decrypted = str(decrypted, encoding="utf-8")
        return decrypted
    else:
        return None

