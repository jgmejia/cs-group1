# Break it Report For Group 3

_by Group 1_

>Group 4  
>[https://cyperpunks.000webhostapp.com/](https://cyperpunks.000webhostapp.com/)  
>[https://bitbucket.org/cyperpunks/cyperpunks/src](https://bitbucket.org/cyperpunks/cyperpunks/src)


## Summary

|Section | Points | 
|:---------|:----------|
|Bugs|    |
|Crashes|    |
|Vulnerabilities|    |
|Basic functionality|    |
|Interface elements|    |
|Security usability|    |
|Total|    |

## Bugs

# Messages were presented more times in the inbox, just by going from outbox to inbox and viceversa, even though they were not repeated in DB. (verified after logging out).
# /dbdump not working (404 error). Found in /user/dbdumb.php

## Crashes 

## Vulnerabilities

* Navigation in a directory through https://cyperpunks.000webhostapp.com/user/

## Basic functionality and usability

### Basic functionality

|Item|Evaluation & points|
|:---------|:----------|
|Code repository is downloadable ?|10|
|Database dump from `/dbdump` is successful ?|0|
|Are messages in the database encrypted?|10|
|Can you access the website ?|10|
|Total|10|

### Interface elements

|Item|Evaluation & points|
|:---------|:----------|
|Is there an index.html page that includes options (or links to pages) to register and to login|0|
|Registration page present|10|
|Registration page functional|10|
|Login page present|10|
|Login works after registration|10|
|Page to see messages sent to the user is present|10|
|Messages to the user are actually shown on the messages page|10|
|Page to send messages is present|10|
|Messages are successfully sent to the recipient|10|
|Total|50|


### Security Usability Analysis

* What are the password requirements?
    * **Min 6 char and one number required**
* Could a user who just wants to access the system as quickly / easily as possible circumvent the security measures with insecure behavior (e.g. a “12345” password, etc.)?
    * **Not very fast as there is verification for each field in the form**
* If there is password-based authentication…
    * How easy is it for a person to memorize this password? Is it easier or harder than a standard 8-character password that requires upper case, lower case, numbers, and punctuation? Justify your answer with principles of memory such as the 7+/-2 rule, chunking, etc. Please indicate More Usable, Equally Usable, Less Usable and provide a reason.
        * **It is easier because it doesn't require the final user to include symbols and there are less chars required.**
* If there is no username-password authentication, but something else in its place…
    * How usable is this alternative to a standard 8-character password? Justify with principles of usability: speed, efficiency, memorability, learnability, user preference. Please indicate More Usable, Equally Usable, Less Usable and provide a reason.
        * **Equally usable, because, even though the requirements in passwords are not very secure, they are enough for the kind of system it is.**
* If there are authentication measures beyond the initial authentication step…
    * What are the additional steps?
        * **Requires one name (5 chars), one email address (valid address with "@", but not verified through sending an email) and a secret answer (6 chars).**
    * How long to the additional steps take to complete (time it)?
        * **If completed normally, it takes one more minute.**
    * How usable are the additional steps? Justify with principles of usability: speed, efficiency, memorability, learnability, user preference.
        * **We found the additional steps very usable, as they all appeared in the same form (making it fast) and indicating the user to complete the required fields in case of a mistake, without removing previous information.**

## Rate the user experience

| | | | | | | |
|-|-|-|-|-|-|-|
|very difficult|1|2|3|4|**5**|very easy|
|very frustrating|1|2|3|**4**|5|very satisfying|
|really terrible|1|2|3|**4**|5|really wonderful|
|very confusing|1|2|3|4|**5**|very clear|
|very slow|1|2|3|**4**|5|very fast|
