# Break it Report For Group 3

_by Group 1_

>Group 3  
>[http://chat4ur.herokuapp.com/](http://chat4ur.herokuapp.com/)  
>[https://github.com/Mostafa-Elsayed94/chhhh](https://github.com/Mostafa-Elsayed94/chhhh)


## Summary

|Section | Points | 
|:---------|:----------|
|Bugs|    |
|Crashes|    |
|Vulnerabilities|    |
|Basic functionality|    |
|Interface elements|    |
|Security usability|    |
|Total|    |

## Bugs

* No email verification provided, users don't have to confirm email or active account from email.

## Crashes

* http://chat4ur.herokuapp.com/dbdump

## Vulnerabilities

## Basic functionality and usability

### Basic functionality

|Item|Evaluation & points|
|:---------|:----------|
|Code repository is downloadable ?|0|
|Database dump from `/dbdump` is successful ?|0|
|Are messages in the database encrypted?|0|
|Can you access the website ?|10|
|Total|10|

### Interface elements

|Item|Evaluation & points|
|:---------|:----------|
|Is there an index.html page that includes options (or links to pages) to register and to login|10|
|Registration page present|10|
|Registration page functional|10|
|Login page present|10|
|Login works after registration|10|
|Page to see messages sent to the user is present|0|
|Messages to the user are actually shown on the messages page|0|
|Page to send messages is present|0|
|Messages are successfully sent to the recipient|0|
|Total|50|


### Security Usability Analysis

* What are the password requirements?
    * **The password must be at least 6 characters.**
* Could a user who just wants to access the system as quickly / easily as possible circumvent the security measures with insecure behavior (e.g. a “12345” password, etc.)?
    * **Yes, User can register a new account with password `111111`.**
* If there is password-based authentication…
    * How easy is it for a person to memorize this password? Is it easier or harder than a standard 8-character password that requires upper case, lower case, numbers, and punctuation? Justify your answer with principles of memory such as the 7+/-2 rule, chunking, etc. Please indicate More Usable, Equally Usable, Less Usable and provide a reason.
        * **Yes it is easy to remember.**
* If there is no username-password authentication, but something else in its place…
    * How usable is this alternative to a standard 8-character password? Justify with principles of usability: speed, efficiency, memorability, learnability, user preference. Please indicate More Usable, Equally Usable, Less Usable and provide a reason.
        * **NO**
* If there are authentication measures beyond the initial authentication step…
    * What are the additional steps?
        * **NO**
    * How long to the additional steps take to complete (time it)?
        * **NO**
    * How usable are the additional steps? Justify with principles of usability: speed, efficiency, memorability, learnability, user preference.
        * **NO**

## Rate the user experience

| | | | | | | |
|-|-|-|-|-|-|-|
|very difficult|1|**2**|3|4|5|very easy|
|very frustrating|**1**|2|3|4|5|very satisfying|
|really terrible|1|2|**3**|4|5|really wonderful|
|very confusing|1|**2**|3|4|5|very clear|
|very slow|1|2|**3**|4|5|very fast|
